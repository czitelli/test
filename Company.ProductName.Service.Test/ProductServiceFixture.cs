using System;
using Company.ProductName.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Company.ProductName.Service.Test
{
    public class ProductServiceFixture: IDisposable
    {
        public ProductService ProductService;
        public OptionsWrapper<ProductServiceConfiguration> options;

        public ProductServiceFixture()
        {
            var condiguration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var productServiceConfig = condiguration.GetSection("Options").Get<ProductServiceConfiguration>();
            options = new OptionsWrapper<ProductServiceConfiguration>(productServiceConfig);
            ProductService = new ProductService(options);
        }

        public void Dispose()
        {

        }
    }
}