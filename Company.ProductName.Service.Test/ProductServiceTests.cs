using System.Threading.Tasks;
using Xunit;

namespace Company.ProductName.Service.Test
{
    public class ProductServiceTests: IClassFixture<ProductServiceFixture>
    {
        private readonly ProductServiceFixture fixture;

        public ProductServiceTests(ProductServiceFixture fix)
        {
            fixture = fix;
        }

        [Fact]
        public async Task GetTest()
        {
            var products = await fixture.ProductService.GetAll();
            Assert.Equal(2, products.Length);
        }
    }
}
