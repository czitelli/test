using System.Threading.Tasks;
using Company.ProductName.Model;
using Newtonsoft.Json;
using Xunit;

namespace Company.ProductName.Service.Test
{
    public class ProductServiceServerTests: IClassFixture<ServerFixture>
    {
        private readonly ServerFixture fixture;

        public ProductServiceServerTests(ServerFixture fix)
        {
            fixture = fix;
        }

        [Fact]
        public async Task GetTest()
        {
            var productsResponse = await fixture.Client.GetAsync("api/products");
            Assert.True(productsResponse.IsSuccessStatusCode);
            var products = JsonConvert.DeserializeObject<Product[]>(await productsResponse.Content.ReadAsStringAsync());
            Assert.Equal(2, products.Length);
        }
    }
}
