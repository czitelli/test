using System;
using System.Net.Http;
using Company.ProductName.Api;
using Company.ProductName.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Company.ProductName.Service.Test
{
    public class ServerFixture: IDisposable
    {
        public TestServer Server;
        public HttpClient Client { get; set; }

        public ServerFixture()
        {
            Server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .ConfigureAppConfiguration(c => c.AddJsonFile("appsettings.json"))
                .ConfigureServices(x =>
                    { }));
            Client = Server.CreateClient();
        }

        public void Dispose()
        {
            Client.Dispose();
            Server.Dispose();
        }
    }
}