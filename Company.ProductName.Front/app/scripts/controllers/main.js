'use strict';

/**
 * @ngdoc function
 * @name companyproductNamefrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the companyproductNamefrontApp
 */
angular.module('companyproductNamefrontApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
