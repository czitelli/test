'use strict';

/**
 * @ngdoc function
 * @name companyproductNamefrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the companyproductNamefrontApp
 */
angular.module('companyproductNamefrontApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
