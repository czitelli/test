﻿using System.Threading.Tasks;
using Company.ProductName.Service;
using Microsoft.AspNetCore.Mvc;

namespace Company.ProductName.Api
{
    public class ProductController : Controller
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet, Route("api/products")]
        public async Task<IActionResult> Get()
        {
            return Ok(await productService.GetAll());
        }
    }
}