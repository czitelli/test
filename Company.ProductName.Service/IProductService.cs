﻿using System.Threading.Tasks;
using Company.ProductName.Model;

namespace Company.ProductName.Service
{
    public interface IProductService
    {
        Task<Product[]> GetAll();

        Task Save(Product product);
    }
}