﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Schema;
using Company.ProductName.Model;
using Microsoft.Extensions.Options;

namespace Company.ProductName.Service
{
    public class ProductService: IProductService
    {
        public string ProductTableName { get; set; }

        public ProductService(IOptions<ProductServiceConfiguration> settings)
        {
            ProductTableName = settings.Value.ProductTableName;
        }

        public Task<Product[]> GetAll()
        {
            return Task.FromResult(new List<Product>()
            {
                new Product() {CategoryId = 2, Name = "Motocicle"},
                new Product() {CategoryId = 2, Name = "Cicle"}
            }.ToArray());
        }

        public Task Save(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
