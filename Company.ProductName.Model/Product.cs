﻿namespace Company.ProductName.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public decimal Price { get; set; }

        public ProductCategory ProductCategory { get; set; }
    }
}