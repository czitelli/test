﻿namespace Company.ProductName.Model
{
    public class ProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}